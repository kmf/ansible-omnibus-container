ansible_omnibus_container
--------------

[ansible-omnibus-container](https://gitlab.com/kmf/ansible-omnibus-container) contains the official PIP install for Ansible files for [ansible](https://pypi.org/project/ansible/).

# Usage

For using latest docker_ansible execute:

```
docker run -v ${PWD}:/opt/ansible --rm kmf/ansible_omnibus_container ansible-playbook --version
```
# Configuration

In the usage example it uses the present(current) working directory `-v ${PWD}:/opt/ansible` to mount to the ansible docker container. `${PWD}` can be change to any directory you like.

# Add Bash Alias

To run ansible easier create an alias in bash:

```sh
alias ansible-playbook="sudo docker run -v ${PWD}:/opt/ansible --rm kmf/ansible_omnibus_container ansible-playbook"
```

# Forked from 
- [docker_ansible](https://gitlab.com/robinlennox/docker_ansible)
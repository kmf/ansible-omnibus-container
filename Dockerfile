FROM python:3.9-slim
LABEL maintainer="Karl Fischer"

RUN apt update && \
    rm -rf /var/lib/apt/lists/* && \
    pip install ansible==6.3.0 ansible-lint pywinrm requests yamllint && \
    pip cache purge && \
    groupadd -r ansible && \
    useradd --no-log-init -m -g ansible ansible && \
    mkdir /opt/ansible && \
    runuser -l ansible -c 'ansible-galaxy collection install community.general ansible.posix ansible.utils ansible.windows community.docker community.digitalocean community.grafana containers.podman chocolatey.chocolatey grafana.grafana'
COPY ansible.cfg /home/ansible/.ansible.cfg
RUN chown -R ansible:ansible /home/ansible/.ansible.cfg
WORKDIR /opt/ansible
COPY entrypoint.sh /home/ansible/entrypoint.sh
ENTRYPOINT ["/bin/sh", "/home/ansible/entrypoint.sh"]
CMD ["ansible-playbook"]
